package build;

public class Carte {
	
	public static enum Couleurs {
		JAVA, DEBIAN, BDD, PHP
	}
	
	public static enum Cartes {
		DEUX, TROIS, QUATRE, CINQ, SIX, SEPT, HUIT, NEUF, DIX, VALET, DAME, ROI, AS
	}
	
	private Couleurs couleur;
	private Cartes numCarte;
	
	public Carte(int coul, int num){
		this.couleur = Couleurs.values()[coul];
		this.numCarte = Cartes.values()[num];
	}

	public int coulToInt() {
		return this.couleur.ordinal();
	}
	
	public int valToInt() {
		return this.numCarte.ordinal();
	}
	
	@Override
	public String toString() {
		return this.numCarte + " de " + this.couleur;
	}
	
	public String getURIFromCard() {
		switch(this.couleur) {
		
			case JAVA:
				return (this.numCarte.ordinal()+2)+"h.gif";
				
			case DEBIAN:
				return (this.numCarte.ordinal()+2)+"d.gif";
				
			case BDD:
				return (this.numCarte.ordinal()+2)+"c.gif";
				
			case PHP:
				return (this.numCarte.ordinal()+2)+"s.gif";
		}
		return null;
	}
}
