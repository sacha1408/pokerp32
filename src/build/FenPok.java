package build;

import java.io.IOException;

import javax.swing.JFrame;

public class FenPok extends JFrame{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private VuePok vue;

	public FenPok() throws IOException {
		this.setSize(1280, 720);
		this.setTitle("Poker");
		this.vue = new VuePok();
		this.add(this.vue);
		this.setVisible(true);
	}
	
	public static void main(String[] args) {
		try {
			new FenPok();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
