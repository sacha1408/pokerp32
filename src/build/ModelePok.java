package build;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

public class ModelePok {
	
	private static int MISE_MIN = 50;
	
	private Deque<Carte> paquet;
	private List<Carte> main;
	private List<Carte> mainD;
	private List<Carte> river;
	private int jetons;
	private int jetonsD;
	private int mise = MISE_MIN;
	
	public void createAndMixPaquet() {
		
		this.paquet = new LinkedList<Carte>();
		
		//cr�e un paquet
		ArrayList<Carte> temp = new ArrayList<Carte>();
		for(int i = 0; i < 4; i++) {
			for(int j = 0; j < 13; j++) {
				Carte carte = new Carte(i, j);
				temp.add(carte);
			}
		}
		//m�lange
		Collections.shuffle(temp);
		for (Carte carte : temp) {
			this.paquet.add(carte);
		}
		/* 
		System.out.println("Contenu du paquet : ");
		for(int i = 0; i < temp.size(); i++) {
			System.out.println(temp.get(i).toString());
		}*/
	}
	
	public ModelePok() {
		this.main = new ArrayList<Carte>(2);
		this.mainD = new ArrayList<Carte>(2);
		this.river = new ArrayList<Carte>(5);
		this.jetons = 1000;
		this.jetonsD = 1000;
		createAndMixPaquet();
		distributeAndRiver();
	}
	
	public List<Carte> getJoueur(){
		return this.main;
	}
	
	public List<Carte> getDealer(){
		return this.mainD;
	}
	
	public List<Carte> getRiver(){
		return this.river;
	}
	
	public int getJetons() {
		return this.jetons;
	}
	
	public int getMise() {
		return this.mise;
	}
	
	public void miseUp(int mise){
		this.mise += mise;
	}
	
	public void distributeAndRiver() {
		for(int i = 0; i < 2; i++) {
			Carte last = this.paquet.pollLast();
			System.out.println("joueur : "+last.toString());
			this.main.add(last);
			Carte last2 = this.paquet.pollLast();
			System.out.println("dealer : "+last2.toString());
			this.mainD.add(last2);
		}
		
		for (int i = 0; i < 5; i++) {
			Carte last = this.paquet.pollLast();
			System.out.println("river "+(i+1)+" : "+last.toString());
			this.river.add(last);
		}
	}
	
	public void lose() {
		this.jetons -= this.mise;
		this.jetonsD += this.mise;
		this.mise = MISE_MIN;
	}
	
	public void win() {
		this.jetons += this.mise;
		this.jetonsD -= this.mise;
		this.mise = MISE_MIN;
	}
}
