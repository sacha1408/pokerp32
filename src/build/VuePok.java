package build;

import java.awt.GridLayout;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class VuePok extends JPanel{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private JPanel mainDealer;
	private JPanel main;
	private JPanel river;
	private JLabel pioche;
	private JLabel ECTS;
	private ModelePok modele;

	public VuePok() throws IOException {
		
		//modele du poker
		this.modele = new ModelePok();
		
		//panel principal pour ne pas tout avoir sur une seule ligne
		JPanel principal = new JPanel(new GridLayout(4, 1, 0, 50));
	
		//main du dealer
		this.mainDealer = new JPanel();
		JLabel carte1 = new JLabel(new ImageIcon(getClass().getClassLoader().getResource("res/card.gif")));
		this.mainDealer.add(carte1);
		JLabel carte2 = new JLabel(new ImageIcon(getClass().getClassLoader().getResource("res/card.gif")));
		this.mainDealer.add(carte2);
		principal.add(this.mainDealer);
		
		//river
		JPanel center = new JPanel(new GridLayout(1,  2));
		this.river = new JPanel(new GridLayout(1, 5));
		
		for(int i = 0; i < 5; i++) {
			if(i < 3) {
				JLabel riv = new JLabel(new ImageIcon(getClass().getClassLoader().getResource("res/"+this.modele.getRiver().get(i).getURIFromCard())));
				this.river.add(riv);
			}else {
				JLabel riv = new JLabel(new ImageIcon(getClass().getClassLoader().getResource("res/vide.png")));
				this.river.add(riv);
			}
		}
		
		center.add(this.river);
		
		//pioche
		this.pioche = new JLabel(new ImageIcon(getClass().getClassLoader().getResource("res/card.gif")));
		center.add(this.pioche);
		
		principal.add(center);
		
		//affichage argent, boutons mise et couche
		JPanel UI = new JPanel(new GridLayout(1, 2));
		JPanel argent = new JPanel();
		JLabel points = new JLabel("ECTS : ");
		this.ECTS = new JLabel(String.valueOf(this.modele.getJetons()));
		argent.add(points);
		argent.add(ECTS);
		
		JPanel buttons = new JPanel();
		JButton mise = new JButton("Miser");
		//mise setOnClickListener
		JButton couche = new JButton("Se coucher");
		//couche setOnClickListener
		buttons.add(mise);
		buttons.add(couche);
		
		UI.add(argent);
		UI.add(buttons);
		principal.add(UI);
		
		//main du dealer
		this.main = new JPanel();
		System.out.println(this.modele.getJoueur().get(0).getURIFromCard());
		JLabel carte3 = new JLabel(new ImageIcon(getClass().getClassLoader().getResource("res/"+this.modele.getJoueur().get(0).getURIFromCard())));
		this.main.add(carte3);
		System.out.println(this.modele.getJoueur().get(1).getURIFromCard());
		JLabel carte4 = new JLabel(new ImageIcon(getClass().getClassLoader().getResource("res/"+this.modele.getJoueur().get(1).getURIFromCard())));
		this.main.add(carte4);
		principal.add(this.main);
		
		this.add(principal);
	}
}
